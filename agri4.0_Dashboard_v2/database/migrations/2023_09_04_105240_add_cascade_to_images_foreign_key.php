<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCascadeToImagesForeignKey extends Migration
{
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            // Supprimez la contrainte existante (si elle existe)
            $table->dropForeign(['id_product']);

            // Ajoutez une nouvelle contrainte avec ON DELETE CASCADE
            $table->foreign('id_product')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            // Supprimez la contrainte existante (si elle existe)
            $table->dropForeign(['id_product']);

            // Ajoutez la contrainte sans ON DELETE CASCADE
            $table->foreign('id_product')
                ->references('id')
                ->on('products');
        });
    }
}
