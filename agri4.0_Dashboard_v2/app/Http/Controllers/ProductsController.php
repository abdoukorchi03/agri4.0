<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

                // create
                    public function create()
                    {
                        return view('Products.create');
                    }


                //index
                    public function index()
                    {
                        $products = Product::with('image')->get();
                        return view("products.Products", compact("products"));
                        //return($products);
                    }
                // Delete
                    public function delete(Request $request)
                    {
                        $product_id = $request->input('product_id');

                        // Votre logique de validation et de suppression du produit
                        // Vous devrez vérifier si l'utilisateur a les autorisations appropriées pour supprimer le produit, etc.

                        // Suppression du produit
                        $product = Product::find($product_id);
                        if (!$product) {
                            // Gérer le cas où le produit n'est pas trouvé
                            return redirect()->back()->with('error', 'Le produit n\'existe pas.');
                        }

                        $product->delete();

                        // Redirection vers la page ou la liste des produits après la suppression
                        return redirect()->route('products.index')->with('success', 'Le produit a été supprimé avec succès.');
                    }
    
                 // Update
                    public function update(Request $request)
                    {
                        $product_id = $request->product_id;
                        $product = Product::with('image')->find($product_id); 
                    
                        if (!$product) {
                            return redirect()->back()->with('error', 'Le produit n\'existe pas.');
                        }
                    
                        return view("products.update", compact("product"));
                        //return ($product);
                    }
            
                // edit
                    public function edit(Request $request)
                    {
                        $product_id = $request->product_id;
                        $product = Product::find($product_id);
                    
                        if (!$product) {
                            return redirect()->back()->with('error', 'Le produit n\'existe pas.');
                        }
                    
                        // Mettez à jour les propriétés du produit
                        $product->libelleP = $request->libelleP;
                        $product->descriptionP = $request->descriptionP;
                        $product->dispon = $request->dispon;
                        $product->prix = $request->prix;
                    
                        // Enregistrez les modifications du produit
                        $product->save();
                    
                        // Gérez les nouvelles images
                        if ($request->hasFile('images')) {
                            foreach ($request->file('images') as $imageFile) {
                                // Obtenez le nom de fichier original
                                $imageName = $imageFile->getClientOriginalName();
                    
                                // Associez la nouvelle image au produit
                                $product->image()->create([
                                    'ImagePath' => $imageName,
                                ]);
                    
                                // Déplacez l'image vers le répertoire de destination (par exemple, public/images)
                                $imageFile->storeAs('images', $imageName, 'public');
                            }
                        }
                    
                        // Gérez les suppressions d'images
                        foreach ($product->image as $image) {
                            $fieldName = 'delete_image_' . $image->id;
                            if ($request->has($fieldName) && $request->input($fieldName) == '1') {
                                // Supprimez l'image
                                $image->delete();
                            }
                        }
                    
                        return redirect()->route('products.index', ['id' => $product_id])->with('success', 'Le produit a été mis à jour avec succès.');
                    }
                // store 
                public function store(Request $request)
                {
                    // Validez les données du formulaire
                    $request->validate([
                        'libelleP' => 'required|string',
                        'descriptionP' => 'required|string',
                        'dispon' => 'required|integer',
                        'prix' => 'required|',
                        'images.*' => 'mimes:jpeg,jpg,png|max:2048', // Validez chaque image individuellement
                    ]);
                
                    // Créez un nouveau produit
                    $product = new Product([
                        'libelleP' => $request->input('libelleP'),
                        'descriptionP' => $request->input('descriptionP'),
                        'dispon' => $request->input('dispon'),
                        'prix' => $request->input('prix'),
                    ]);

                
                    // Enregistrez le produit
                    $product->save();
                
                    // Traitez les images téléchargées
                    if ($request->hasFile('images')) {
                        foreach ($request->file('images') as $image) {
                            $newImagePath = $image->store('images'); // Enregistrez l'image dans le stockage
                            $productImage = new Image([
                                'ImagePath' => $newImagePath,
                            ]);
                            $product->images()->save($productImage); // Associez l'image au produit
                        }
                    }
                
                    return redirect()->route('products.index')->with('success', 'Produit ajouté avec succès.');
                }
                  
                // details
                public function details(Request $request)
                {
                    $product_id = $request->product_id;
                    
                    // Utilisez la méthode with pour charger la relation 'images'
                    $productDetails = Product::with('image')->find($product_id);
                
                    if (!$productDetails) {
                        // Gérer le cas où le produit n'est pas trouvé
                        return response()->json(['message' => 'Produit non trouvé'], 404);
                    }
                    else
                
                    return view("products.details",compact("productDetails"));
                }
                
                
                
                
                
    
}
