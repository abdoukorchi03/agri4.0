@extends('layouts.app')
@section('title', "agri4.0 || Clients")
@section('content')
<link rel="stylesheet" href="{{ asset('styles/users.css') }}">


<div>
    @section('h1', "Clients")
 
 <body>
     <main class="table">
         <section class="table__header">
             <div class="input-group">
                 <input type="search" placeholder="Search Data..."  >
                 <img src="images/search.png" alt="">
             </div>
             <div class="export__file">
                 <label for="export-file" class="export__file-btn" title="Export File"></label>
                 <input type="checkbox" id="export-file">
                 <div class="export__file-options">
                     <label>Export As &nbsp; &#10140;</label>
                     <label for="export-file" id="toPDF">PDF <img src="images/pdf.png" alt=""></label>
                     <label for="export-file" id="toJSON">JSON <img src="images/json.png" alt=""></label>
                     <label for="export-file" id="toCSV">CSV <img src="images/csv.png" alt=""></label>
                     <label for="export-file" id="toEXCEL">EXCEL <img src="images/excel.png" alt=""></label>
                 </div>
             </div>
         </section>
         
         <section class="table__body">
             <table>
                 <thead>
                     <tr>
                         <th> Customer <span class="icon-arrow">&UpArrow;</span></th>
                         <th> Email <span class="icon-arrow">&UpArrow;</span></th>
                         <th> Order Date <span class="icon-arrow">&UpArrow;</span></th>
                         <th> Status <span class="icon-arrow">&UpArrow;</span></th>
                         <th> Amount <span class="icon-arrow">&UpArrow;</span></th>
                     </tr>
                 </thead>
                 <tbody>
                    @foreach ($users as $u)
                    
                    <tr>
                       
                      <td >  <img src="{{ asset('images/' . $u->photo) }}" >{{$u->name}}</td>
                        <td>{{$u->email}} </td>
                        <td> {{$u->created_at}} </td>
                        <td>
                            <p class="status delivered">Delivered</p>
                        </td>
                        <td> <strong> $128.90 </strong></td>
                    </tr>
                
                    @endforeach
                    
                    
                 </tbody>
             </table>
         </section>
     </main>
 </body>
 <script src="{{ asset('js/Users.js') }}"></script>


 </html>
</a>
@endsection
