@extends('layouts.app')
@section('h1')
     Product
@endsection
@section('title', 'agri4.0 || Products')

@section('content')
<a href="/create"><button class="btn btn-primary">Add Product</button></a>
<div class="album py-5 bg-body-tertiary">
   

    <div class="container">
       
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-3">
            @foreach($products as $p)
            <div class="col">
                <div class="card shadow-sm">
                    @if($p->image && count($p->image) > 0)
                    <img src="{{ asset('images/' . $p->image[0]->ImagePath) }}" class="bd-placeholder-img card-img-top" width="100%" height="225">
                    @else
                    <img src="{{ asset('images/default.jpg') }}" class="bd-placeholder-img card-img-top"  width="100%" height="225">
                    @endif
                    <div class="card-body">
                        <h4 style="color: rgb(13, 4, 49)">{{$p->libelleP}}</h4>
                        <!-- Description courte -->
                        <p class="card-text" id="shortDescription{{$p->id}}">
                            {{ Str::limit($p->descriptionP, 100, '...') }}
                            <span id="longDescription{{$p->id}}" style="display:none;">{{$p->descriptionP}}</span>
                        </p>
                        
                        <!-- Bouton "Lire la suite" -->
                        <button class="btn btn-link" onclick="toggleDescription({{$p->id}})">Lire la suite</button>
                        <div class="d-flex justify-content-between align-items-center">
                            <b><p class="card-text">{{$p->prix}}.00 Dhs</p></b>
                            <div class="btn-group">
                                {{-- Button Edit --}}
                                <form action="Products/update" method="post">
                                    @csrf
                                    <input type="hidden" name="product_id" name="id" value="{{$p->id}}">
                                    <button type="submit" class="btn btn-sm btn-primary">Edit</button>
                                </form>

                                {{-- Button Show --}}
                                <form action="Products/details" method="post">
                                    @csrf
                                    <input type="hidden" name="product_id" name="id" value="{{$p->id}}">
                                    <button type="submit" class="btn btn-sm btn-warning">View</button>
                                </form>

                                <form action="Products/delete" method="post">
                                    @csrf
                                    <input type="hidden" name="product_id" name="id" value="{{$p->id}}">
                                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer ce produit ?')">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<script>
    function toggleDescription(productId) {
        var shortDescription = document.getElementById("shortDescription" + productId);
        var longDescription = document.getElementById("longDescription" + productId);

        if (shortDescription.style.display === "none") {
            shortDescription.style.display = "inline";
            longDescription.style.display = "none";
        } else {
            shortDescription.style.display = "none";
            longDescription.style.display = "inline";
        }
    }
</script>

@endsection
