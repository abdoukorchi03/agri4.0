@extends('layouts.app')
@section('h1')
     Details Product {{$productDetails->libelleP}}
@endsection
@section('title', 'agri4.0 ||  Details Product')

@section('content')
<style>
        .body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .container {
            padding: 20px;
            background-color: #ffffff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .product-details {
            margin-top: 20px;
        }

        .product-image {
            max-width: 100%;
            height: auto;
        }

        .product-description {
            margin-top: 20px;
        }
</style>

</head>
<body class="body">
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-6">
                <h2>{{$productDetails->libelleP}}</h2>
                <p class="text-muted">{{$productDetails->descriptionP}}</p>
                <p>Prix: {{$productDetails->prix}} Dhs</p>
            </div>
            <div class="col-md-6">
                <div class="product-images-slider">
                    @if ($productDetails->image && count($productDetails->image) > 0)
                        @foreach($productDetails->image as $image)
                            <div>
                                <img src="{{ asset('images/' . $image->ImagePath) }}" alt="Image du produit" class="img-fluid product-image">
                            </div>
                        @endforeach
                    @else
                        <p>Aucune image disponible</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    

    <!-- Inclure Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>

@endsection