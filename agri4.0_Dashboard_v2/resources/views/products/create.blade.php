
@extends('layouts.app')
                @section('h1')
                    Create a new  Product
                @endsection
@section('title', ' agri4.0 || Add Product')



@section('content')


<div class="container mt-3">
    <form action="store" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="libelleP" class="form-label">Libellé Produit</label>
            <input type="text" class="form-control" id="libelleP" name="libelleP" required>
        </div>

        <div class="mb-3">
            <label for="descriptionP" class="form-label">Description du Produit</label>
            <textarea class="form-control" id="descriptionP" name="descriptionP" required></textarea>
        </div>

        <div class="mb-3">
            <label for="dispon" class="form-label">Disponibilité</label>
            <input type="number" class="form-control" id="dispon" name="dispon" required>
        </div>

        <div class="mb-3">
            <label for="prix" class="form-label">Prix</label>
            <input type="number" class="form-control" id="prix" name="prix" required>
        </div>

        <!-- Ajoutez ici un champ pour télécharger une seule image -->
        <div class="mb-3">
            <label for="image" class="form-label">Image du Produit</label>
            <input type="file" class="form-control" id="image" name="image" accept="image/*">
        </div>

       {{--  <!-- Ajoutez ici un champ pour télécharger plusieurs images à la fois -->
        <div class="mb-3">
            <label for="images" class="form-label">Images du Produit (plusieurs)</label>
            <input type="file" class="form-control" id="images" name="images[]" multiple accept="image/*">
        </div> --}}

        <!-- Ajoutez d'autres champs pour votre modèle Product ici -->

        <button type="submit" class="btn btn-primary">Ajouter le produit</button>
    </form>
</div>
@endsection