<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('enregistrements', function (Blueprint $table) {
            $table->foreignId('product_id')->constrained("products");
            $table->foreignId('panier_id')->constrained("paniers");
            $table->integer("quantite");
            $table->primary(["product_id" , "panier_id"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('enregistrements');
    }
};
