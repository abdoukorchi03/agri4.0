<?php

use App\Http\Controllers\ProductsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::get("/",function()
            {
                return view("layouts\app");
            } );




            // Products 
Route::get('/',[ProductsController::class,"index"])->name("products.index");
Route::post('/Products/delete',[ProductsController::class,"delete"]);
Route::post('/Products/update',[ProductsController::class,"update"]);
Route::post('/Products/edit',[ProductsController::class,"edit"]);
Route::get('/create',[ProductsController::class,"create"]);
Route::post('store',[ProductsController::class,"store"]);
Route::post('/Products/details',[ProductsController::class,"details"])->name("products.show");















Route::get('/Users',[UsersController::class,"index"]);
