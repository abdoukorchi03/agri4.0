/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/views/Products.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}