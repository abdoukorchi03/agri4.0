@section('title', 'Ajouter Produit')
@extends('layouts.app')

<body>
    <div class="container">
        <h1 class="mt-4">Mise à jour du produit: {{$product->libelleP}}</h1>
        <form action='edit' method="POST" enctype="multipart/form-data"> <!-- Ajoutez enctype="multipart/form-data" pour la gestion des fichiers -->
            @csrf
            <input type="hidden" class="form-control" id="product_id" name="product_id" value="{{$product->id}}">

            <div class="mb-3">
                <label for="libelleP" class="form-label">Libellé Produit</label>
                <input type="text" class="form-control" id="libelleP" name="libelleP" value="{{$product->libelleP}}">
            </div>

            <div class="mb-3">
                <label for="descriptionP" class="form-label">Description du Produit</label>
                <textarea class="form-control" id="descriptionP" name="descriptionP">{{$product->descriptionP}}</textarea>
            </div>

            <div class="mb-3">
                <label for="dispon" class="form-label">Disponibilité</label>
                <input type="number" class="form-control" id="dispon" name="dispon" value="{{$product->dispon}}">
            </div>

            <div class="mb-3">
                <label for="NombreImage" class="form-label">Nombre d'Images</label>
                <input type="number" class="form-control" id="NombreImage" name="NombreImage" value="{{$product->NombreImage}}">
            </div>

            <div class="mb-3">
                <label for="images" class="form-label">Nouvelles Images du Produit</label>
                <input type="file" class="form-control" id="images" name="images[]" multiple>
            </div>

            <h3>Images existantes :</h3>
            @if ($product->image)
                @foreach($product->image as $image)
                    <div class="mb-3">
                        <img src="{{ asset( 'images/' .$image->ImagePath) }}" alt="{{$image->ImagePath}}" style="max-width: 100px;">
                        <label for="image_{{$image->id}}" class="form-label">Modifier l'image</label>
                        <input type="file" class="form-control" id="image_{{$image->id}}" name="image_{{$image->id}}">
                        <label for="delete_image_{{$image->id}}">Supprimer l'image</label>
                        <input type="checkbox" id="delete_image_{{$image->id}}" name="delete_image_{{$image->id}}" value="1">
                    </div>
                @endforeach
            @else
                Aucune image existante.
            @endif
            

            <button type="submit" class="btn btn-primary">Mettre à jour</button>
        </form>
    </div>
    <!-- Utilisation de Bootstrap pour les scripts JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
</body>

