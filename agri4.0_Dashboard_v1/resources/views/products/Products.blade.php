@extends('layouts.app')

@section('title', 'Products')

<style>
    /* Styles pour les images des cartes */
    .card-img {
        max-height: 200px; /* Hauteur maximale des images */
        object-fit: cover; /* Redimensionnement des images pour remplir la zone */
    }

    /* Styles pour la description */
    .card-description {
        max-height: 1.5em; /* Hauteur maximale d'une ligne et demie */
        overflow: hidden; /* Masquer le texte dépassant de la hauteur maximale */
        text-overflow: ellipsis; /* Ajouter "..." à la fin du texte coupé */
    }
</style>

<div class="container mt-4">
    <div class="row">
        @foreach($products as $p)
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    @if($p->image && count($p->image) > 0)
                        <img src="{{ asset('images/' . $p->image[0]->ImagePath) }}" alt="Image du produit" class="card-img-top card-img">
                    @else
                        <div class="text-center mt-3">
                            Aucune image
                        </div>
                    @endif
                    <div class="card-body">
                        <h5 class="card-title">{{$p->libelleP}}</h5>
                        <p class="card-text card-description">{{$p->descriptionP}}</p>
                        <p class="card-text">{{$p->prix}} Dhs</p>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-between">
                            {{-- Delete Button --}}
                            <form action="Products/delete" method="post">
                                @csrf
                                <input type="hidden" name="product_id" name="id" value="{{$p->id}}">
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer ce produit ?')">Delete</button>
                            </form>
                    
                            {{-- Update Button --}}
                            <form action="Products/update" method="post">
                                @csrf
                                <input type="hidden" name="product_id" name="id" value="{{$p->id}}">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>

                            <form action="Products/details" method="post">
                                @csrf
                                <input type="hidden" name="product_id" name="id" value="{{$p->id}}">
                                <button type="submit" class="btn btn-warning">Details</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
