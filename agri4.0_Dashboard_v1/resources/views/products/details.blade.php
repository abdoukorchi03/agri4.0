<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Détails du Produit</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
@extends('layouts.app')

@section('title', 'Products')
    <style>
        .body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #ffffff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        h1 {
            font-size: 24px;
            color: #333;
        }

        .product-details {
            margin-top: 20px;
        }

        .product-image {
            max-width: 100%;
            height: auto;
        }

        .product-description {
            margin-top: 20px;
        }
    </style>
</head>
<body class="body">
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-center">Détails du Produit</h1>
                <hr>
                <h2>{{$productDetails->libelleP}}</h2>
                <p class="text-muted">{{$productDetails->descriptionP}}</p>
                <p>Prix: {{$productDetails->prix}} Dhs</p>
            </div>
            <div class="col-md-6">
                <div class="product-images-slider">
                    @if ($productDetails->image && count($productDetails->image) > 0)
                        @foreach($productDetails->image as $image)
                            <div>
                                <img src="{{ asset('images/' . $image->ImagePath) }}" alt="Image du produit" class="img-fluid product-image">
                            </div>
                        @endforeach
                    @else
                        <p>Aucune image disponible</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    <script>
        $(document).ready(function(){
            $('.product-images-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000, // Réglez la vitesse de défilement
                dots: true, // Afficher les indicateurs de pagination
                infinite: true
            });
        });
    </script>
    
</body>
</html>
