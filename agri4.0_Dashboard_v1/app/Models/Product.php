<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'libelleP',
        'descriptionP',
        'dispon',
        'prix'
        // Ajoutez d'autres champs ici s'ils sont nécessaires
    ];

    function image()
    {
        return $this->hasMany(Image::class, 'id_product');
    }
}
