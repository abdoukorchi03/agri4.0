<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ['ImagePath', 'id_product'];
    public function produit()
    {
        return $this->belongsTo(Product::class, 'id_product');
    }



}
